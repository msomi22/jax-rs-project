/**
 * 
 */
package ke.co.alandick.messenger.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author peter
 *
 */
public class ConsumeRest {

	/**
	 * @param args
	 * 
	 * http://localhost:8086/jax-rs-project/webapi/messages
	 * 
	 */
	public static void main(String[] args) {

		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://localhost:8086/jax-rs-project/webapi/messages");

			ClientResponse response = webResource.accept("application/xml")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			String output = response.getEntity(String.class);
			xmlParser(output); 
			//System.out.println("Output from Server .... \n");
			//System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
	
	/**
	 * 
	 * @param xmlStr
	 * @throws ParserConfigurationException
	 * @throws UnsupportedEncodingException
	 * @throws SAXException
	 * @throws IOException
	 */

	public static void xmlParser(String xmlStr) throws ParserConfigurationException, 
	                                    UnsupportedEncodingException, SAXException, IOException {
		
		   //System.out.println("xmlStr: "+ xmlStr); 

			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			DocumentBuilder b = f.newDocumentBuilder();
			Document doc = b.parse(new ByteArrayInputStream(xmlStr.getBytes("UTF-8")));
			
			NodeList messages = doc.getElementsByTagName("message");
			
			for (int i = 0; i < messages.getLength(); i++) {
				Element message = (Element) messages.item(i);
				
				Node id = message.getElementsByTagName("id").item(0);
				Node author = message.getElementsByTagName("author").item(0);
				Node messge = message.getElementsByTagName("messageDesc").item(0);
				Node created = message.getElementsByTagName("created").item(0);
				
				System.out.println("************************************");
				
				System.out.println("id: " + id.getTextContent());
				System.out.println("author: " + author.getTextContent());
				System.out.println("messge: " + messge.getTextContent());
				System.out.println("created: " + created.getTextContent());
				
				
				
			}
		
	}

	}

	/**
	 * 
	 *     <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
			<messages>
			    <message>
			        <author>Peter</author>
			        <created>2017-04-03T10:35:18.337+03:00</created>
			        <id>1</id>
			        <messageDesc>Hello Dennis</messageDesc>
			    </message>
			    <message>
			        <author>Dennis</author>
			        <created>2017-04-03T10:35:18.337+03:00</created>
			        <id>2</id>
			        <messageDesc>Yes Peter</messageDesc>
			    </message>
			</messages>
	 * 
	 */

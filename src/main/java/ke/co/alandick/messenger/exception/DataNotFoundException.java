/**
 * 
 */
package ke.co.alandick.messenger.exception;

/**
 * @author peter
 *
 */
public class DataNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7534650423703633848L;

	/**
	 * 
	 */
	public DataNotFoundException(String mesage) {
		super(mesage);
	}

}

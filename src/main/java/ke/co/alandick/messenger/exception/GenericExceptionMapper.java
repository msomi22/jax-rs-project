/**
 * 
 */
package ke.co.alandick.messenger.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import ke.co.alandick.messenger.model.ErrorMessage;

/**
 * @author peter
 *
 */
//@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable>{
	
		@Override
		public Response toResponse(Throwable e) {
			ErrorMessage errorMessage = new ErrorMessage(e.getMessage(),500,"http://www.alandick.co.ke/doc"); 
			return Response.status(Status.INTERNAL_SERVER_ERROR) 
					.entity(errorMessage)
					.build();
		}

}

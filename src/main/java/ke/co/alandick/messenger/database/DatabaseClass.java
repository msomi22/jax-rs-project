/**
 * 
 */
package ke.co.alandick.messenger.database;

import java.util.*;

import ke.co.alandick.messenger.model.Message;
import ke.co.alandick.messenger.model.Profile;

/**
 * @author peter
 *
 */
public class DatabaseClass {
	
	public static Map<Long, Message> message = new HashMap<Long, Message>();
	public static Map<String, Profile> profile = new HashMap<String, Profile>();
	
	
	public static Map<Long, Message> getMessage() {
		return message;
	}
	
	
	public static Map<String, Profile> getProfile() {
		return profile;
	}
	

}

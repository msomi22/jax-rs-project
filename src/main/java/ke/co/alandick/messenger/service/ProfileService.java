/**
 * 
 */
package ke.co.alandick.messenger.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ke.co.alandick.messenger.model.Profile;

/**
 * @author peter
 *
 */
public class ProfileService {

public static  Map<String, Profile> profiles = new HashMap<String, Profile>();
	
	public ProfileService(){
		profiles.put("PeterProfile", new Profile(1L,"PeterProfile","Peter","Mwenda"));
		profiles.put("DennisProfile", new Profile(2L,"DennisProfile","Dennis","Mutegi"));
	}

	public List<Profile> getAllProfiles(){
		return new ArrayList<Profile>(profiles.values());
	}
	
	public Profile getProfile(String profileName){
		return profiles.get(profileName);
	}
	
	public Profile addProfile(Profile profile){
		profile.setId(profiles.size() + 1); 
		profiles.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Profile updateProfile(Profile profile){
		if(profile.getProfileName().isEmpty()){
			return null;
		}
		profiles.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Profile removeProfile(String profileName){
		return profiles.remove(profileName);
	}

}

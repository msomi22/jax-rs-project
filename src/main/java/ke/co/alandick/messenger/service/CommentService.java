/**
 * 
 */
package ke.co.alandick.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import ke.co.alandick.messenger.database.DatabaseClass;
import ke.co.alandick.messenger.model.Comment;
import ke.co.alandick.messenger.model.ErrorMessage;
import ke.co.alandick.messenger.model.Message;

/**
 * @author peter
 *
 */
public class CommentService {
	
	private  Map<Long, Message> messages = DatabaseClass.getMessage();

	public List<Comment> getAllComments(long messageId){
		Map<Long,Comment> comments = messages.get(messageId).getComents();
		comments.put(1L, new Comment(1,"Peter","Nice"));
		comments.put(1L, new Comment(2,"Dennis","haha!"));
		return new ArrayList<Comment>(comments.values());
	}
	
	public Comment getComment(long messageId,long commentId){
		
		ErrorMessage errorMessage = new ErrorMessage("Not found",404,"http://www.alandick.co.ke/doc"); 
		Response response= Response.status(Status.NOT_FOUND) 
				.entity(errorMessage)
				.build();
		
		Message message =  messages.get(messageId);
		
		if(message == null){
			throw new WebApplicationException(response);
		} 
		
		Map<Long,Comment> comments = messages.get(messageId).getComents();
		Comment comment = comments.get(commentId);
		
		if(comment == null){
			throw new NotFoundException(response);
		} 
		
		return comment;
	}
	
	public Comment addComment(long messageId,Comment comment){
		Map<Long,Comment> comments = messages.get(messageId).getComents();
		comment.setId(comments.size() + 1); 
		comments.put(comment.getId(), comment);
		return comment;
	}
	
	public Comment updateComment(long messageId, Comment comment){
		Map<Long,Comment> comments = messages.get(messageId).getComents();
		if(comment.getId() <= 0){
			return null;
		}
		comments.put(comment.getId(), comment);
		return comment;
	}
	
	public Comment removeComment(long messageId,long commentId){
		Map<Long,Comment> comments = messages.get(messageId).getComents();
		return comments.remove(commentId);
	}
	
}

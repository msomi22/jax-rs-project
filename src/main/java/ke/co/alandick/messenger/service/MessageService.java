/**
 * 
 */
package ke.co.alandick.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import ke.co.alandick.messenger.database.DatabaseClass;
import ke.co.alandick.messenger.exception.DataNotFoundException;
import ke.co.alandick.messenger.model.Message;

/**
 * @author peter
 * 
 * http://localhost:8086/jax-rs-project/webapi/messages
 *
 */
public class MessageService {
	
	private  Map<Long, Message> messages = DatabaseClass.getMessage();
	
	public MessageService(){
		messages.put(1L, new Message(1,"Hello Dennis" , "Peter"));
		messages.put(2L, new Message(2,"Yes Peter" , "Dennis"));
	}
	
	public List<Message> getAllMessagesYear(int year){
		List<Message> messageListYear =  new ArrayList<Message>();
		Calendar cal = Calendar.getInstance();
		for(Message mssg : messages.values()){
			cal.setTime(mssg.getCreated());
			if(cal.get(Calendar.YEAR) == year){
				messageListYear.add(mssg);
			}
		}
		return messageListYear;
	}
	
	public List<Message> getAllMessagesPaginated(int start, int size){
		ArrayList<Message>  list = new ArrayList<Message>(messages.values());
		if(start + size > list.size()) return  new ArrayList<Message>(); 
		return list.subList(start, start + size); 
	}
	

	public List<Message> getAllMessages(){
		return new ArrayList<Message>(messages.values());
	}
	
	public Message getMessage(long id){
		Message message = messages.get(id);
		if(message == null){
			throw new DataNotFoundException("Message with id " + id + " not found");
		}
		return message;
	}
	
	public Message addMessage(Message message){
		message.setId(messages.size() + 1); 
		messages.put(message.getId(), message);
		return message;
	}
	
	public Message updateMessage(Message message){
		if(message.getId() <= 0){
			return null;
		}
		messages.put(message.getId(), message);
		return message;
	}
	
	public Message removeMessage(long id){
		return messages.remove(id);
	}
	

}

/**
 * 
 */
package ke.co.alandick.messenger.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import ke.co.alandick.messenger.model.Message;
import ke.co.alandick.messenger.resources.beans.MessageFilterBean;
import ke.co.alandick.messenger.service.MessageService;

/**
 * @author peter
 *
 */

@Path("/messages") 
@Consumes(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML,MediaType.APPLICATION_XML})
@Produces(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML,MediaType.APPLICATION_XML}) 
public class MessageResource {
	
	private MessageService messageService = new MessageService();

	@GET
	public List<Message> getMessage(@BeanParam MessageFilterBean Filterbean){ 
		if(Filterbean.getYear() > 0){
			return messageService.getAllMessagesYear(Filterbean.getYear());
		}
		
		if(Filterbean.getStart() > 0 && Filterbean.getSize() > 0){
			
			return messageService.getAllMessagesPaginated(Filterbean.getStart(), Filterbean.getSize());
		}
		
		return messageService.getAllMessages();
	}
	
	
	@GET
	@Path("/{messageId}")
	public Message getMessage(@PathParam("messageId") long id , @Context UriInfo uriInfo){ 
		Message message = messageService.getMessage(id);
		message.addLinks(getSelfUrl(uriInfo, message), "self");
		message.addLinks(getProfileUrl(uriInfo, message), "profile");
		message.addLinks(getCommentsUrl(uriInfo, message), "comments");
		return message;
	}


	/**
	 * @param uriInfo
	 * @param message
	 * @return
	 */
	private String getCommentsUrl(UriInfo uriInfo, Message message) {
		String uri = uriInfo.getBaseUriBuilder()
				   .path(MessageResource.class)
			       .path(MessageResource.class, "getCommentsResource")
			       .path(CommentsResource.class)
			       .resolveTemplate("messageId", message.getId())
			       .build()
			       .toString();
			return uri;
	}


	/**
	 * @param uriInfo
	 * @param message 
	 * @return
	 */
	private String getProfileUrl(UriInfo uriInfo, Message message) {
		String uri = uriInfo.getBaseUriBuilder()
		       .path(ProfileResource.class)
		       .path(message.getAuthor())
		       .build()
		       .toString();
		return uri;
	}


	/**
	 * @param uriInfo
	 * @param message
	 * @return
	 */
	private String getSelfUrl(UriInfo uriInfo, Message message) {
		String uri = uriInfo.getBaseUriBuilder()
		       .path(MessageResource.class)
		       .path(Long.toString(message.getId()))
		       .build()
		       .toString();
		return uri;
	}
	
	
	
	@POST
	public Response addMessage(Message message, @Context UriInfo uriInfo){
		Message newMessage = messageService.addMessage(message);
		String newId = String.valueOf(newMessage.getId());
		URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build(); 
		return Response.created(uri)
		               .entity(newMessage)
		               .build();
		
		//return messageService.addMessage(message);
	}
	
	
	@PUT
	@Path("/{messageId}")
	public Message updateMessage(@PathParam("messageId") long id, Message message){
		message.setId(id); 
		return messageService.updateMessage(message);
	}
	
	@DELETE
	@Path("/{messageId}")
	public void removeMessage(@PathParam("messageId") long id){
		 messageService.removeMessage(id);
	}
	
	@Path("/{messageId}/comments")
	public CommentsResource getCommentsResource(){
		return new CommentsResource();
	}
	
	
	@GET
	@Path("/header")
	public String getHeaderParam(@HeaderParam("param1") String param1 , @CookieParam("cookie") String cookie){
		return "param1 is : " + param1 + " , cookie is : " + cookie;
	}
	
	@GET
	@Path("/context")
	public String getConext(@Context UriInfo urlinfo, @Context HttpHeaders httpheaders){
		System.out.println(urlinfo.getQueryParameters());
		System.out.println(urlinfo.getPathParameters());
		System.out.println(urlinfo.getAbsolutePath().toString());
		
		System.out.println(httpheaders.getAcceptableLanguages());
		System.out.println(httpheaders.getCookies());
		System.out.println(httpheaders.getDate());
		System.out.println(httpheaders.getLanguage());
		System.out.println(httpheaders.getMediaType());
		System.out.println(httpheaders.getRequestHeaders());
		
		
		return "put put";
	}
	
	
	
}

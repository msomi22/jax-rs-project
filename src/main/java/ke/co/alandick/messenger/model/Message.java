/**
 * 
 */
package ke.co.alandick.messenger.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author peter
 *
 */

@XmlRootElement 
public class Message {
	
	private long id;
	private String messageDesc;
	private Date created;
	private String author;
	private Map<Long,Comment> coments = new HashMap<Long,Comment>();
	private List<Link> links = new ArrayList<Link>();
	
	public Message(){
		
	}


	public Message(long id, String messageDesc, String author) {
		super();
		this.id = id;
		this.messageDesc = messageDesc;
		this.created = new Date();
		this.author = author;
		this.coments =  new HashMap<Long,Comment>();
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	

	public String getMessageDesc() {
		return messageDesc;
	}


	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
    @XmlTransient 
	public Map<Long, Comment> getComents() {
		return coments;
	}


	public void setComents(Map<Long, Comment> coments) {
		this.coments = coments;
	}


	public List<Link> getLinks() {
		return links;
	}


	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
	public void addLinks(String url, String rel){
		Link link = new Link();
		link.setLink(url);
		link.setRel(rel);
		links.add(link);
	}


	@Override
	public String toString() {
		return "Message [id=" + id + ", message=" + messageDesc + ", created="
				+ created + ", author=" + author + "]";
	}
	

}

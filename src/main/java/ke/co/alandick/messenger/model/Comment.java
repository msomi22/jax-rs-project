/**
 * 
 */
package ke.co.alandick.messenger.model;

import java.util.Date;

/**
 * @author peter
 *
 */
public class Comment {
	
	private long id;
	private String message;
	private Date created;
	private String author;
	
	public Comment(){
		
	}
	/**
	 * @param id
	 * @param message
	 * @param created
	 * @param author
	 */
	public Comment(long id, String message,String author) {
		super();
		this.id = id;
		this.message = message;
		this.created = new Date();
		this.author = author;
	}
	
	
	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	@Override
	public String toString() {
		return "Comment [id=" + id + ", message=" + message + ", created="
				+ created + ", author=" + author + "]";
	}
	

}
